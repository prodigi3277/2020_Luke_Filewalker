/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.*;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;

public class DeathWheel extends SubsystemBase {
  private TalonSRX beybladeMotor;
  private DigitalInput outputSensor;
  private DigitalInput outputSensor2;
  private static int TICKS_PER_SLOT = 1050;
  private int currentSlotTicks = 0;
  private int deathWheelBallCount = 0;
  private boolean isBallAdvancing = false;
  private NetworkTableInstance tableInstance;
  private NetworkTableEntry ballsInDeathWheelEntry;
  private NetworkTableEntry feederToDeathWheel;
  private boolean outputSensorBefore;
  private boolean outputSensor2Before;
  private NetworkTableEntry shootEntry;
  private NetworkTableEntry isBallStuckInShooter;
  private NetworkTableEntry addToDeathWheelCountEntry;
  private boolean isBallAdvancingManually;
  private static double BEYBLADE_SPEED = 0.5;
  private static double BEYBLADE_WITH_SHOOTER_SPEED = 0.6;

  public DeathWheel() {
    try {
      beybladeMotor = new TalonSRX(Constants.DW_TALON);
    } catch (Exception e) {
      System.out.println("beyblade motor not working");
    }
    try {
      outputSensor = new DigitalInput(Constants.WHEEL_OUT_SWITCH);
    } catch (Exception e) {
      System.out.println("output sensor not working");
    }
    try {
      outputSensor2 = new DigitalInput(Constants.WHEEL_OUT_SWITCH_TWO);
    } catch (Exception e) {
      System.out.println("output sensor 2 not working");
    }

    tableInstance = NetworkTableInstance.getDefault();
    NetworkTable table = tableInstance.getTable(Constants.GLOBAL_NETWORK_TABLE);

    ballsInDeathWheelEntry = table.getEntry(Constants.DW_COUNT_TABLE_KEY);
    feederToDeathWheel = table.getEntry(Constants.FEEDER_DEATHWHEEL_KEY);
    shootEntry = table.getEntry(Constants.IS_SHOOTER_SHOOTING_KEY);
    addToDeathWheelCountEntry = table.getEntry(Constants.ADD_TO_DW_COUNT);
    isBallStuckInShooter = table.getEntry(Constants.IS_SHOOTER_STUCK_KEY);

    beybladeMotor.configFactoryDefault();
    final int timeoutBeyblade = 30;
    beybladeMotor.configSelectedFeedbackSensor(FeedbackDevice.QuadEncoder, 0, timeoutBeyblade);
    beybladeMotor.setSelectedSensorPosition(0, 0, timeoutBeyblade);
  }

  private void countBalls() {

    if (deathWheelBallCount <= 0) {
      // Just in case there is negativity
      deathWheelBallCount = 0;
    }

    double addToDeathWheel = addToDeathWheelCountEntry.getDouble(0);
    addToDeathWheelCountEntry.setDouble(0);

    deathWheelBallCount += addToDeathWheel;

    if (outputSensor.get() && !outputSensorBefore || outputSensor2.get() && !outputSensor2Before) {
      outputSensorBefore = true;
      outputSensor2Before = true;
      addToDeathWheelCountEntry.setNumber(-1);
    }

    SmartDashboard.putNumber("deathWheelBallCount", deathWheelBallCount);

    ballsInDeathWheelEntry.setDouble(deathWheelBallCount);
  }

  public void reverseEverythingDeathWheel() {
    beybladeMotor.set(ControlMode.PercentOutput, -BEYBLADE_SPEED);
  }

  public void stopThingsReversingDeathWheel() {
    beybladeMotor.set(ControlMode.PercentOutput, 0);

  }

  public void deathWheelAdvanceManually() {
    isBallAdvancingManually = !isBallAdvancingManually;
    if (!isBallAdvancingManually) {
      beybladeMotor.set(ControlMode.PercentOutput, BEYBLADE_SPEED);
      isBallAdvancingManually = true;
    } else {
      beybladeMotor.set(ControlMode.PercentOutput, 0);
      isBallAdvancingManually = false;
    }
  }

  public void ballQueueing() {
    countBalls();
    boolean isBallShooting = shootEntry.getBoolean(false);
    // if (isBallShooting && outputSensor.get() && outputSensorBefore) {
    //   isBallStuckInShooter.setBoolean(true);
    // }
    if (isBallShooting) {
      beybladeMotor.set(ControlMode.PercentOutput, BEYBLADE_WITH_SHOOTER_SPEED);
      isBallAdvancing = true;
      currentSlotTicks = beybladeMotor.getSelectedSensorPosition(0);
    } else {
      boolean isThereBallAvailable = feederToDeathWheel.getBoolean(false);
      if (isThereBallAvailable) {
        isBallAdvancing = true;
        beybladeMotor.set(ControlMode.PercentOutput, BEYBLADE_SPEED);
      }

      if (isBallAdvancing) {
        if (beybladeMotor.getSelectedSensorPosition(0) > currentSlotTicks + TICKS_PER_SLOT) {

          beybladeMotor.set(ControlMode.PercentOutput, 0);
          feederToDeathWheel.setBoolean(false);
          isBallAdvancing = false;
          currentSlotTicks = beybladeMotor.getSelectedSensorPosition(0);
        }
      }
    }
  }
}
