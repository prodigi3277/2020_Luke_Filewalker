/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import com.revrobotics.ColorSensorV3;
import com.revrobotics.ColorMatchResult;
import com.ctre.phoenix.motorcontrol.can.*;
import com.revrobotics.ColorMatch;

public class MeSeeColorGood extends SubsystemBase {
  private final I2C.Port i2cPort = I2C.Port.kOnboard;
  private final ColorSensorV3 m_colorSensor = new ColorSensorV3(i2cPort);
  /**
   * A Rev Color Match object is used to register and detect known colors. This
   * can be calibrated ahead of time or during operation.
   * 
   * This object uses a simple euclidian distance to estimate the closest match
   * with given confidence range.
   */
  private final ColorMatch m_colorMatcher = new ColorMatch();

  /**
   * Note: Any example colors should be calibrated as the user needs, these are
   * here as a basic example.
   */
  private final Color kBlueTarget = ColorMatch.makeColor(0.143, 0.427, 0.429);
  private final Color kGreenTarget = ColorMatch.makeColor(0.197, 0.561, 0.240);
  private final Color kRedTarget = ColorMatch.makeColor(0.561, 0.232, 0.114);
  private final Color kYellowTarget = ColorMatch.makeColor(0.361, 0.524, 0.113);
  private int countImportantColor = 0;
  private WPI_TalonSRX colorWheel;
  private Color currentColor = Color.kBurlywood;
  private Color previousColor = Color.kBurlywood;
  private Color startColor = Color.kPapayaWhip;
  private Color gameColorTarget = Color.kBurlywood;
  private String gameData;
  private boolean isBarRequestedDown = false;
  private boolean isPhaseTwoComplete = false;
  private boolean debugModeEnabled = true;
  private boolean isFinished = false;
  private double CONFIDENCE_THRESHOLD = 0.95;
  private double currentColorConfidence = 0;

  /**
   * Creates a new MeSeeColorGood.
   */
  public MeSeeColorGood() {
    m_colorMatcher.addColorMatch(kBlueTarget);
    m_colorMatcher.addColorMatch(kGreenTarget);
    m_colorMatcher.addColorMatch(kRedTarget);
    m_colorMatcher.addColorMatch(kYellowTarget);

    try {
      colorWheel = new WPI_TalonSRX(Constants.CW_TALON);
    } catch (Exception e) {
      System.out.println("Color wheel not working");
    }
    colorWheel.configFactoryDefault();
  }

  private void getDetectedColor() {
    /**
     * The method GetColor() returns a normalized color value from the sensor and
     * can be useful if outputting the color to an RGB LED or similar. To read the
     * raw color, use GetRawColor().
     * 
     * The color sensor works best when within a few inches from an object in well
     * lit conditions (the built in LED is a big help here!). The farther an object
     * is the more light from the surroundings will bleed into the measurements and
     * make it difficult to accurately determine its color.
     */
    Color detectedColor = m_colorSensor.getColor();

    /**
     * Run the color match algorithm on our detected color
     */
    ColorMatchResult match = m_colorMatcher.matchClosestColor(detectedColor);
    currentColor = match.color;
    currentColorConfidence = match.confidence;
  }

  private boolean colorMatched(Color colorMatched) {
    return colorMatched == kBlueTarget || colorMatched == kRedTarget || colorMatched == kGreenTarget
        || colorMatched == kYellowTarget;
  }

  private void getStartingColor() {
    if (colorMatched(currentColor) && !colorMatched(startColor)) {
      startColor = currentColor;
      colorWheel.set(0.2);
    }
  }

  private void getGameData() {
    gameData = DriverStation.getInstance().getGameSpecificMessage();
  }

  private void countStartOccurances() {
    if (currentColorConfidence >= CONFIDENCE_THRESHOLD) {
      if (currentColor != previousColor && colorMatched(startColor) && colorMatched(currentColor)
          && currentColor == startColor) {
        countImportantColor += 1;
      }

      previousColor = currentColor;
    }
  }

  private void setGameDefinedColorTarget() {
    getGameData();
    if (gameData.length() > 0) {
      switch (gameData.charAt(0)) {
      case 'B':
        gameColorTarget = kRedTarget;
        break;
      case 'G':
        gameColorTarget = kYellowTarget;
        break;
      case 'R':
        gameColorTarget = kBlueTarget;
        break;
      case 'Y':
        gameColorTarget = kGreenTarget;
        break;
      default:
        // TODO: log this
        break;
      }
    }
  }

  private void debugLogging() {
    if (debugModeEnabled) {
      String startColorString = "";
      String currentColorString = "";
      String previousColorString = "";

      if (startColor == kBlueTarget) {
        startColorString = "blue";
      } else if (startColor == kRedTarget) {
        startColorString = "red";
      } else if (startColor == kGreenTarget) {
        startColorString = "green";
      } else if (startColor == kYellowTarget) {
        startColorString = "yellow";
      }

      if (currentColor == kBlueTarget) {
        currentColorString = "blue";
      } else if (currentColor == kRedTarget) {
        currentColorString = "red";
      } else if (currentColor == kGreenTarget) {
        currentColorString = "green";
      } else if (currentColor == kYellowTarget) {
        currentColorString = "yellow";
      }

      if (previousColor == kBlueTarget) {
        previousColorString = "blue";
      } else if (previousColor == kRedTarget) {
        previousColorString = "red";
      } else if (previousColor == kGreenTarget) {
        previousColorString = "green";
      } else if (previousColor == kYellowTarget) {
        previousColorString = "yellow";
      }

      SmartDashboard.putString("Important color", startColorString);
      SmartDashboard.putNumber("color count", countImportantColor);
      SmartDashboard.putString("current color", currentColorString);
      SmartDashboard.putString("previous color", previousColorString);
      // SmartDashboard.putNumber("t/f",input.get() ? 1 : 0);
    }
  }

  public boolean wheelSpinnyColor() {
    getDetectedColor();
    if (!isPhaseTwoComplete) {
      getStartingColor();
      countStartOccurances();

      debugLogging();

      if (countImportantColor >= 8 && isFinished == false) {
        isPhaseTwoComplete = true;
        colorWheel.set(0);
        return true;
      }
    } else {
      getGameData();
      setGameDefinedColorTarget();

      colorWheel.set(0.1);
      if (currentColor == gameColorTarget) {
        isFinished = true;
        colorWheel.set(0);
        return true;
      }
    }

    return false;
  }
}