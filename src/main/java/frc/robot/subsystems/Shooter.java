/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

import com.ctre.phoenix.motorcontrol.StatorCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.TalonFXControlMode;
import com.ctre.phoenix.motorcontrol.can.*;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.networktables.*;


public class Shooter extends SubsystemBase {
  private TalonFX rightShooterMotor;
  private TalonFX leftShooterMotor;
  private WPI_TalonSRX upDownShooterMotor;
  private static double SHOOTER_UP_DOWN_VALUE = 0.5;
  private boolean shooterUpDownToggle = false;
  private Servo cameraServo;
  private static double SERVO_UP_DEGREES = 240;
  private static double SERVO_DOWN_DEGREES = 50;
  private NetworkTableInstance tableInstance;
  private NetworkTableEntry totalBallsInRobot;
  private Number MOVE_SHOOTER_WHEN_HAS_BALL = 4;
  private NetworkTableEntry areWeShooting;
  private Servo shooterAssistServo;
  private NetworkTableEntry isBallStuckInShooter;
  private boolean isBallStuckInShooterBoolean;
  private static double SHOOTER_SPEED = 0.93;


  public Shooter(XboxController controller) {
    try {
      rightShooterMotor = new WPI_TalonFX(Constants.RS_TALON);
    } catch (Exception e) {
      System.out.println("RSM not working");
    }
    try {
      leftShooterMotor = new WPI_TalonFX(Constants.LS_TALON);
    } catch (Exception e) {
      System.out.println("LSM not working");
    }
    try {
      cameraServo = new Servo(Constants.CAMERA_SERVO);
    } catch (Exception e) {
      System.out.println("camera servo not working");
    }
    try {
      shooterAssistServo = new Servo(Constants.SHOOTER_ASSIST_SERVO); 
      } catch (Exception e) {
        System.out.println("shooter assist servo not working");
      }
    
  
    try {
      upDownShooterMotor = new WPI_TalonSRX(Constants.UDSM_TALON);
    } catch (Exception e) {
      System.out.println("up/down shooter not working");
    }


    rightShooterMotor.configFactoryDefault();
    leftShooterMotor.configFactoryDefault();

    rightShooterMotor.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 150, 175, 5));
    leftShooterMotor.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 150, 175, 5));

    rightShooterMotor.setInverted(true);
    rightShooterMotor.follow(leftShooterMotor);
    tableInstance = NetworkTableInstance.getDefault();
    NetworkTable table = tableInstance.getTable(Constants.GLOBAL_NETWORK_TABLE);
    totalBallsInRobot = table.getEntry(Constants.TOTAL_TABLE_KEY);
    areWeShooting = table.getEntry(Constants.IS_SHOOTER_SHOOTING_KEY);
    isBallStuckInShooter =table.getEntry(Constants.IS_SHOOTER_STUCK_KEY);

  }

  public void shootyShoot(double shooterSpeed) {
    if(shooterSpeed > 0){
  isBallStuckInShooterBoolean = isBallStuckInShooter.getBoolean(false);

    areWeShooting.setBoolean(true);
   System.out.println("shooter enabled");
    leftShooterMotor.set(TalonFXControlMode.PercentOutput, SHOOTER_SPEED);
    cameraServo.setAngle(SERVO_UP_DEGREES);
  
 if (isBallStuckInShooterBoolean) {
   shooterAssistServo.setAngle(120);
 }
}
 
  }

  // public void shootyShootAuto(){
  //   if(totalBallsInRobot.getNumber(0) == MOVE_SHOOTER_WHEN_HAS_BALL){
  //     boolean entry = true;
  //    System.out.println("shooter enabled");
  //     leftShooterMotor.set(TalonFXControlMode.PercentOutput, 0.88);
  //     cameraServo.setAngle(SERVO_UP_DEGREES);}
   
 // }

  public void shootyStop() {
    areWeShooting.setBoolean(false);
    
    System.out.println("shooter stopped");
    leftShooterMotor.set(TalonFXControlMode.PercentOutput, 0.0);
    cameraServo.setAngle(SERVO_DOWN_DEGREES);
  }

  public void shooterUp(){

   upDownShooterMotor.set(SHOOTER_UP_DOWN_VALUE);
 }

 public void shooterDown(){
  upDownShooterMotor.set(-SHOOTER_UP_DOWN_VALUE);
 }

 public void shooterUpDownStop(){
   upDownShooterMotor.set(0);
 }
}
