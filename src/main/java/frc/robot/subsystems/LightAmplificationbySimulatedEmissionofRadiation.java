/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import edu.wpi.first.wpilibj.Timer;

public class LightAmplificationbySimulatedEmissionofRadiation extends SubsystemBase {
  private AddressableLED dasGummyBear;
  private AddressableLED dasGummyBear2;
  private AddressableLEDBuffer lightBuffer = new AddressableLEDBuffer(Constants.LED_AMOUNT);
  private AddressableLEDBuffer lightBuffer2 = new AddressableLEDBuffer(Constants.LED_AMOUNT);

  private DriverStation ds;

  private int m_rainbowFirstPixelHue = 0;

  public LightAmplificationbySimulatedEmissionofRadiation() {

    ds = DriverStation.getInstance();
    try {
      dasGummyBear = new AddressableLED(Constants.PWM_LEDS);
    } catch (Exception e) {
      System.out.println("led 1 not working");
    }
    try {
      dasGummyBear2 = new AddressableLED(Constants.PWM_LEDS2);
    } catch (Exception e) {
      System.out.println("led 2 not working");
    }

    dasGummyBear.setLength(lightBuffer.getLength());
    // dasGummyBear2.setLength(lightBuffer2.getLength());
  }

  private void setRGB(int red, int green, int blue) {
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      // Sets the specified LED to the RGB values for red

      lightBuffer.setRGB(i, red, green, blue);
    }

    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  private void setBlueRgb(int index) {
    lightBuffer.setRGB(index, 0, 0, 255);
  }

  private void setRedRgb(int index) {
    lightBuffer.setRGB(index, 255, 0, 0);
  }

  private void setGreenRgb(int index) {
    lightBuffer.setRGB(index, 0, 255, 0);
  }

  private void setYellowRgb(int index) {
    lightBuffer.setRGB(index, 255, 255, 0);
  }

  private void setOffRgb(int index) {
    lightBuffer.setRGB(index, 0, 0, 0);
  }

  public void flashingColor() {
    for (var lights = 0; lights < 10; lights++) {
      setRedRgb(lights);
      Timer.delay(0.1);
      setOffRgb(lights);
      Timer.delay(0.1);
    }
  }

  public void setRGBReverse() {
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      if (i < 3) {
        setYellowRgb(i);
      } else if (i > 3 && i < Constants.LED_AMOUNT - 3) {
        if (ds.getAlliance() == DriverStation.Alliance.Red)
          setRedRgb(i);
        else if (ds.getAlliance() == DriverStation.Alliance.Blue) {
          setBlueRgb(i);
        }
      } else if (i >=  Constants.LED_AMOUNT - 3) {
        setGreenRgb(i);
      }
    }

    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  // public void setRGBReverse2() {
  //   for (var i = 0; i < lightBuffer.getLength(); i++) {
  //     if (i < 3) {
  //       setYellowRgb(i);
  //     } else if (i > 3 && i < 57) {
  //       if (ds.getAlliance() == DriverStation.Alliance.Red)
  //         setRedRgb(i);
  //       else if (ds.getAlliance() == DriverStation.Alliance.Blue) {
  //         setBlueRgb(i);
  //       }
  //     } else if (i >= 57) {
  //       setGreenRgb(i);
  //     }
  //   }

  //   dasGummyBear2.setData(lightBuffer);
  //   dasGummyBear2.start();
  // }

    // https://www.youtube.com/watch?v=dQw4w9WgXcQ i think this might help


  public void setRGBForward() {
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      if (i < 3) {
        setGreenRgb(i);
      } else if (i > 3 && i < Constants.LED_AMOUNT - 3) {
        if (ds.getAlliance() == DriverStation.Alliance.Red) {
          setRedRgb(i);
        } else if (ds.getAlliance() == DriverStation.Alliance.Blue) {
          setBlueRgb(i);
        }
      } else if (i >= Constants.LED_AMOUNT - 3) {
        setYellowRgb(i);
      }
    }

    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  // public void setRGBForward2() {
  //   for (var i = 0; i < lightBuffer.getLength(); i++) {
  //     if (i < 3) {
  //       setGreenRgb(i);
  //     } else if (i > 3 && i < 57) {
  //       if (ds.getAlliance() == DriverStation.Alliance.Red) {
  //         setRedRgb(i);
  //       } else if (ds.getAlliance() == DriverStation.Alliance.Blue) {
  //         setBlueRgb(i);
  //       }
  //     } else if (i >= 57) {
  //       setYellowRgb(i);
  //     }
  //   }

  //   dasGummyBear2.setData(lightBuffer);
  //   dasGummyBear2.start();
  // }

  public void setOff() {
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      setOffRgb(i);
    }
    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  public void setTeamColor() {
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      if (ds.getAlliance() == DriverStation.Alliance.Red)
          setRedRgb(i);
        else if (ds.getAlliance() == DriverStation.Alliance.Blue) {
          setBlueRgb(i);
        }
    }
    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  // public void setTeamColor2(){
  // for (var i = 0; i < lightBuffer.getLength(); i++) {
  // ds.getAlliance();
  // }
  // dasGummyBear2.setData(lightBuffer);
  // dasGummyBear2.start();
  // }

  public void rainbow() {
    // For every pixel
    for (var i = 0; i < lightBuffer.getLength(); i++) {
      // Calculate the hue - hue is easier for rainbows because the color
      // shape is a circle so only one value needs to precess
      final var hue = (m_rainbowFirstPixelHue + (i * 180 / lightBuffer.getLength())) % 180;
      // Set the value
      lightBuffer.setHSV(i, hue, 255, 128);
    }
    // Increase by to make the rainbow "move"
    m_rainbowFirstPixelHue += 3;
    // Check bounds
    m_rainbowFirstPixelHue %= 180;

    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }

  public void popoMode() {
    for (var lights = 0; lights < 10; lights++) {
      setRGB(255, 0, 0);
      Timer.delay(0.1);
      setRGB(0, 0, 255);
      Timer.delay(0.1);
    }

    dasGummyBear.setData(lightBuffer);
    dasGummyBear.start();
  }
}
