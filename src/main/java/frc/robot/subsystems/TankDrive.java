
package frc.robot.subsystems;

import com.analog.adis16470.frc.ADIS16470_IMU;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.StatorCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.OI;
import edu.wpi.first.wpilibj.AnalogGyro;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class TankDrive extends SubsystemBase {
  private DifferentialDrive _differentialDrive;
  private WPI_TalonFX frontLeftMotor;
  private WPI_TalonFX frontLeftMotor2;

  private WPI_TalonSRX frontLeftMotorProto;
  private WPI_TalonSRX backLeftMotor;
  private WPI_TalonFX frontRightMotor;
  private WPI_TalonFX frontRightMotor2;

  private WPI_TalonSRX frontRightMotorProto;
  private WPI_TalonSRX backRightMotor;
  private static double FINESSE_REDUCTION_RATE = 0.4;
  private boolean isFinesseMode = false;
  private static SpeedControllerGroup driveLeft;
  private static SpeedControllerGroup driveRight;

  // public ADIS16470_IMU imu;
  private static final double kAngleSetpoint = 0.0;
  private static final double kP = 0.005;
  private static OI m_oi;

  public TankDrive(OI oi) {
    super();
    m_oi = oi;

    try {
      frontLeftMotor = new WPI_TalonFX(Constants.FLM_TALON);
      frontLeftMotor.configFactoryDefault();
      frontLeftMotor.setInverted(false);
    } catch (Exception e) {
      System.out.println("FLM not working");
    }
    try {
      frontLeftMotor2 = new WPI_TalonFX(Constants.FLM_TALON2);
      frontLeftMotor2.configFactoryDefault();
      frontLeftMotor2.setInverted(false);
    } catch (Exception e) {
      System.out.println("FLM2 not working");
    }

    // try {
    // imu = new ADIS16470_IMU();
    // } catch (Exception e) {
    // System.out.println(" imu not working");
    // }

    try {
      frontRightMotor = new WPI_TalonFX(Constants.FRM_TALON);
      frontRightMotor.configFactoryDefault();
      frontRightMotor.setInverted(true);
    } catch (Exception e) {
      System.out.println("FRM failed");
    }

    try {
      frontRightMotor2 = new WPI_TalonFX(Constants.FRM_TALON2);
      frontRightMotor2.configFactoryDefault();
      frontRightMotor2.setInverted(true);
    } catch (Exception e) {
      System.out.println("FRM2 failed");
    }

    driveLeft = new SpeedControllerGroup(frontLeftMotor, frontLeftMotor2);
    driveRight = new SpeedControllerGroup(frontRightMotor, frontRightMotor2);

    // imu.reset();
    _differentialDrive = new DifferentialDrive(driveLeft, driveRight);
    driveLeft.setInverted(true);

    frontLeftMotor.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 150, 175, 5));
    frontRightMotor.configStatorCurrentLimit(new StatorCurrentLimitConfiguration(true, 150, 175, 5));

    frontLeftMotor.setSelectedSensorPosition(0);
  }

  public void finesseToggle() {
    isFinesseMode = !isFinesseMode;
  }

  // private double gyroAssistDriveStraight(double turn) {
  // double turningValue = (kAngleSetpoint - imu.getAngle()) * kP;
  // // Invert the direction of the turn if we are going backwards
  // turningValue = Math.copySign(turningValue, turn);
  // return turningValue;
  // }

  public double autonPathDriveForward() {
    driveRight.set(0.2);
    driveLeft.set(-0.2);
    return frontRightMotor.getSelectedSensorPosition(0);
  }

  public void autonPathStop() {
    driveRight.set(0);
    driveLeft.set(0);
  }

  public void DriveWithController(double leftSpeed, double rightSpeed) {
    if (isFinesseMode) {
      leftSpeed = leftSpeed - (leftSpeed * FINESSE_REDUCTION_RATE);
      rightSpeed = rightSpeed - (rightSpeed * FINESSE_REDUCTION_RATE);
    }
    if (rightSpeed == 0) {
      // rightSpeed = gyroAssistDriveStraight(rightSpeed);
    } else {
      // imu.reset();
    }
    SmartDashboard.putNumber("rightSpeed", rightSpeed);

    _differentialDrive.arcadeDrive(leftSpeed, rightSpeed);

  }

}
