/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.LimitSwitchNormal;
import com.ctre.phoenix.motorcontrol.LimitSwitchSource;
import com.ctre.phoenix.motorcontrol.can.*;
import frc.robot.Constants;

public class Feeder extends SubsystemBase {
  private WPI_TalonSRX feederBarRoller;
  private WPI_TalonSRX feederBarUpDown;
  private WPI_TalonSRX ballStagingLeft;
  private WPI_TalonSRX ballStagingRight;
  private DigitalInput frontFirstSwitch;
  private DigitalInput frontSecondSwitch;
  private DigitalInput backFirstSwitch;
  private DigitalInput backSecondSwitch;
  private static double FEEDER_BAR_SPEED = 0.5;
  private int directionMode = 0;
  private int previousDirectionMode = 0;
  private int ballsInFeeder = 0;
  private static double FEEDER_BELT_SPEED = 0.4;
  private static double FEEDER_UP_DOWN_SPEED = 0.2;
  private static double MAKE_FEEDER_BAR_STAY_DOWN_SPEED = 0.05;
  private NetworkTableEntry ballsInFeederEntry;
  private NetworkTableEntry totalEntry;
  private NetworkTableInstance tableInstance;
  private NetworkTableEntry addToDeathWheelCountEntry;
  private NetworkTableEntry feederToDeathWheel;
  private boolean systemSaysStopLoadingBalls = false;
  private boolean isOkLoadBall = false;
  private boolean frontFirstSwitchBefore;
  private boolean frontSecondSwitchBefore;
  private boolean backFirstSwitchBefore;
  private boolean backSecondSwitchBefore;
  private boolean beltForward;
  private boolean feederBarHeightToggle = true;
  private boolean m_debugPrint = true;
  private boolean isRequestedDown = false;
  private boolean feederBarLowerLimitSwitchBefore = false;

  public Feeder() {

    super();

    try {
      feederBarRoller = new WPI_TalonSRX(Constants.FB_TALON_ROLL);
    } catch (Exception e) {
      System.out.println("FB not working");
    }

    try {
      feederBarUpDown = new WPI_TalonSRX(Constants.FUD_TALON);
    } catch (Exception e) {
      System.out.println("FB not working");
    }

    try {
      ballStagingLeft = new WPI_TalonSRX(Constants.BSL_TALON);
    } catch (Exception e) {
      System.out.println("BSL not working");
    }

    try {
      ballStagingRight = new WPI_TalonSRX(Constants.BSR_TALON);
    } catch (Exception e) {
      System.out.println("BSR not working");
    }

    try {
      frontFirstSwitch = new DigitalInput(Constants.FEEDER_FRONT_FIRST_SWITCH);
    } catch (Exception e) {
      System.out.println("front first switch not working");
    }

    try {
      frontSecondSwitch = new DigitalInput(Constants.FEEDER_FRONT_SECOND_SWITCH);
    } catch (Exception e) {
      System.out.println("front second switch not working");
    }

    try {
      backFirstSwitch = new DigitalInput(Constants.FEEDER_BACK_FIRST_SWITCH);
    } catch (Exception e) {
      System.out.println("back first switch not working");
    }

    try {
      backSecondSwitch = new DigitalInput(Constants.FEEDER_BACK_SECOND_SWITCH);
    } catch (Exception e) {
      System.out.println("back second switch not working");
    }

    SmartDashboard.putString("DB/String 0", "0.0");
    SmartDashboard.putString("DB/String 1", "0.0");

    feederBarRoller.configFactoryDefault();
    feederBarUpDown.configFactoryDefault();
    // feederBarUpDown.set(-FEEDER_UP_DOWN_SPEED);
    ballStagingLeft.configFactoryDefault();
    ballStagingRight.configFactoryDefault();
    feederBarUpDown.configForwardLimitSwitchSource(LimitSwitchSource.FeedbackConnector,
        LimitSwitchNormal.NormallyClosed, 0);
    feederBarUpDown.configReverseLimitSwitchSource(LimitSwitchSource.FeedbackConnector,
        LimitSwitchNormal.NormallyClosed, 0);

    feederBarRoller.setInverted(true);
    ballStagingRight.setInverted(true);
    ballStagingRight.follow(ballStagingLeft);
    final int timeoutBallStaging = 30;
    ballStagingLeft.configSelectedFeedbackSensor(FeedbackDevice.QuadEncoder, 0, timeoutBallStaging);
    ballStagingLeft.setSelectedSensorPosition(0, 0, timeoutBallStaging);

    tableInstance = NetworkTableInstance.getDefault();
    NetworkTable table = tableInstance.getTable(Constants.GLOBAL_NETWORK_TABLE);
    ballsInFeederEntry = table.getEntry(Constants.FEEDER_COUNT_TABLE_KEY);
    totalEntry = table.getEntry(Constants.TOTAL_TABLE_KEY);

    // Use with handoff control between belt and death wheel
    feederToDeathWheel = table.getEntry(Constants.FEEDER_DEATHWHEEL_KEY);
    addToDeathWheelCountEntry = table.getEntry(Constants.ADD_TO_DW_COUNT);
  }

  // Evaluate whether or not we should load a ball.
  // The total balls loaded in the bot shall not exceed 5
  private void canBallLoad() {
    double totalBalls = totalEntry.getDouble(0);

    isOkLoadBall = totalBalls <= Constants.MAX_NUM_BALLS_ALLOWED_HOLDING;
  }

  // Count the balls entering the robot. Subtract when balls leaving via the
  // feeder.
  private void countBalls() {
    // First actually count the balls before modifying the tracking states
    // Handling the front sensor set
    if (!frontFirstSwitch.get() && !frontSecondSwitch.get() && !frontFirstSwitchBefore && frontSecondSwitchBefore) {
      ballsInFeeder++;
      beltForward = false;
    } else if (!frontFirstSwitch.get() && !frontSecondSwitch.get() && frontFirstSwitchBefore
        && !frontSecondSwitchBefore) {
      ballsInFeeder--;
      beltForward = true;
    }
    // Handling the rear sensor set
    if (!backFirstSwitch.get() && !backSecondSwitch.get() && !backFirstSwitchBefore && backSecondSwitchBefore) {
      ballsInFeeder--;
      addToDeathWheelCountEntry.setNumber(1);
    } else if (!backFirstSwitch.get() && !backSecondSwitch.get() && backFirstSwitchBefore && !backSecondSwitchBefore) {
      // Not really common, but in case a ball gets put back in from the death wheel
      ballsInFeeder++;
      addToDeathWheelCountEntry.setNumber(-1);
    }

    // After counting its safe to change the tracking states
    // front switches first
    if (frontFirstSwitch.get() && !frontFirstSwitchBefore) {
      frontFirstSwitchBefore = true;
    }
    if (frontSecondSwitch.get() && !frontSecondSwitchBefore) {
      frontSecondSwitchBefore = true;
    }
    if (!frontFirstSwitch.get() && frontFirstSwitchBefore) {
      frontFirstSwitchBefore = false;
    }
    if (!frontSecondSwitch.get() && frontSecondSwitchBefore) {
      frontSecondSwitchBefore = false;
    }
    // then the back switches
    if (backFirstSwitch.get() && !backFirstSwitchBefore) {
      backFirstSwitchBefore = true;
    }
    if (backSecondSwitch.get() && !backSecondSwitchBefore) {
      backSecondSwitchBefore = true;
    }
    if (!backFirstSwitch.get() && backFirstSwitchBefore) {
      backFirstSwitchBefore = false;
    }
    if (!backSecondSwitch.get() && backSecondSwitchBefore) {
      backSecondSwitchBefore = false;
    }

    if (ballsInFeeder < 0) {
      // Less than zero is bad... better to be zero than negative!
      ballsInFeeder = 0;
    }
  if(!isRequestedDown){
    feederBarUpDown.set(-FEEDER_UP_DOWN_SPEED);
  }
    SmartDashboard.putNumber("ballsInFeeder", ballsInFeeder);

    ballsInFeederEntry.setDouble(ballsInFeeder);
  }

  public void reverseEverythingFeeder() {
    feederBarUpDown.set(-FEEDER_UP_DOWN_SPEED);
    ballStagingLeft.set(-FEEDER_BELT_SPEED);
  }

  public void stopThingsReversingFeeder() {
    feederBarUpDown.set(0);
    ballStagingLeft.set(0);

  }
 
  // This is run all the time
  private void setFeederBarDirection() {
    int preservePreviousDirectionMode = directionMode;
    // DO NOT injest balls if lifted off limit switch
    if (feederBarUpDown.isFwdLimitSwitchClosed() != 0 && directionMode == 1) {
      directionMode = 0;
    }

     if (feederBarUpDown.isFwdLimitSwitchClosed() == 0 && !feederBarLowerLimitSwitchBefore && isRequestedDown) {
       directionMode = 0;
     }

    if (feederBarUpDown.isFwdLimitSwitchClosed() == 0) {
      feederBarLowerLimitSwitchBefore = true;
      // isRequestedDown = false;
      directionMode = 1;
    } else if (feederBarLowerLimitSwitchBefore && feederBarUpDown.isFwdLimitSwitchClosed() != 0) {
      feederBarLowerLimitSwitchBefore = false;
    }

    switch (directionMode) {
      case 1:
        feederBarRoller.set(FEEDER_BAR_SPEED);
        break;
      case -1:
        feederBarRoller.set(-FEEDER_BAR_SPEED);
        break;
      default:
        feederBarRoller.set(0);
        break;
    }

    directionMode = preservePreviousDirectionMode;
  }

  // This is meant to be controlled by the driver
  public void changeMode() {
    if (directionMode == 1) {
      previousDirectionMode = 1;
      directionMode = 0;
    } else if (directionMode == -1) {
      previousDirectionMode = -1;
      directionMode = 0;
    } else if (directionMode == 0) {
      if (previousDirectionMode == 1) {
        previousDirectionMode = 0;
        directionMode = -1;
      } else if (systemSaysStopLoadingBalls == false) {
        previousDirectionMode = 0;
        directionMode = 1;
      }
    }
  }

  public void stopPickingUpBalls(double totalBalls) {
    if (totalBalls == Constants.MAX_NUM_BALLS_ALLOWED_HOLDING && systemSaysStopLoadingBalls == false) {
      systemSaysStopLoadingBalls = true;
      feederBarRoller.set(-FEEDER_BAR_SPEED);
      directionMode = -1;
      previousDirectionMode = 1;
      changeMode();
    } else {
      systemSaysStopLoadingBalls = false;
      feederBarRoller.set(FEEDER_BAR_SPEED);
      directionMode = 1;
      changeMode();
    }
  }

  public void feederOnStart() {
    feederBarUpDown.set(-FEEDER_UP_DOWN_SPEED);
  }

  public void feederBarLift() {
    feederBarHeightToggle = !feederBarHeightToggle;
    if (feederBarHeightToggle) {
      feederBarUpDown.set(FEEDER_UP_DOWN_SPEED);
      isRequestedDown = true;
    } else {
      feederBarUpDown.set(-FEEDER_UP_DOWN_SPEED);
      isRequestedDown = false;
    }
  }
//make feeder bar stay down but leave room for balls
  private void feederBarLiftEndMotorDown() {
    if (feederBarUpDown.isFwdLimitSwitchClosed() == 0 && isRequestedDown) {
      feederBarUpDown.set(0);
    }
  }

  public void feederBeltControl() {
    countBalls();
    feederBarLiftEndMotorDown();
    setFeederBarDirection();
    canBallLoad();

    DebugPrint();

    boolean isDeathWheelRunning = feederToDeathWheel.getBoolean(false);

    if (isOkLoadBall && feederBarUpDown.isFwdLimitSwitchClosed() == 0) {
      //feeder bar is down
      ballStagingLeft.set(FEEDER_BELT_SPEED);
      directionMode = 1;
      if (backFirstSwitch.get() || backSecondSwitch.get() || feederToDeathWheel.getBoolean(false)) {
        //this means ball is going to death grater
        //TODO: make it work better
        feederToDeathWheel.setBoolean(true);
        SmartDashboard.putString("DB/String 1", "0.0");
      }
     
    } else {
      //feeder bar is up
      ballStagingLeft.set(0);
      directionMode = 0;
    }
  }

  private void DebugPrint() {
    if (m_debugPrint) {
      SmartDashboard.putString("FrontSensor1", Boolean.toString(frontFirstSwitch.get()));
      SmartDashboard.putString("FrontSensor2", Boolean.toString(frontSecondSwitch.get()));
      SmartDashboard.putString("BackSensor1", Boolean.toString(backFirstSwitch.get()));
      SmartDashboard.putString("BackSensor2", Boolean.toString(backSecondSwitch.get()));
      SmartDashboard.putString("isOkLoadBall", Boolean.toString(isOkLoadBall));
      SmartDashboard.putString("feederToDeathWheel", Boolean.toString(feederToDeathWheel.getBoolean(false)));
      SmartDashboard.putNumber("LeftFeederBeltAmps", ballStagingLeft.getStatorCurrent());
    }
  }
}
