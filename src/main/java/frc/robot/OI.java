/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

public class OI {
    private double joystickLeftY = 0;
    private double joystickRightX = 0;
    private double triggerRight = 0;
    private XboxController m_controller;
    private double deadZoneX = 0.07;
    private double deadZoneY = 0.15;

    public OI(XboxController controller) {
        m_controller = controller;
    }

    private void getLeft() {
        joystickLeftY = m_controller.getY(Hand.kLeft);
    }

    private void rightTrigger(){
       triggerRight = m_controller.getTriggerAxis(Hand.kRight);
    }

    private void getRight() {
        joystickRightX = m_controller.getX(Hand.kRight);
    }
    
   
     //Link the buttons to commands
    
    
    
    public void updateControllers() {
        getLeft();
        getRight();
    }

    public void deadZone() {
        double tempX;
        double tempY;

        tempX = Math.abs(joystickRightX);
        tempY = Math.abs(joystickLeftY);
        if (tempY < deadZoneY) {
            joystickLeftY = 0;
        }
        if (tempX < deadZoneX) {
            joystickRightX = 0;
        }
    }

    public double getRightX()
    {
        return joystickRightX;
    }

    public double getLeftY()
    {
        return joystickLeftY;
    }

    public double getTriggerRight(){
        rightTrigger();
        return triggerRight;
    }
}
