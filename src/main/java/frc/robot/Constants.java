/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class Constants {
//answers to the man from scene 24
    public static int AVG_AIRSPEED_VELOCITY_OF_AN_UNLADEN_EUROPEAN_SWALLOW = 11; //in meters per second
    public static String CAPITAL_OF_ASSYRIA = "Nineveh";

    // Motor controllers
    public static int UP_CLIMB_MOTOR = 50;
    public static int DOWN_CLIMB_MOTOR = 51;
    public static int FLM_TALON2 = 10;
    public static int FLM_TALON = 11;
    public static int FRM_TALON = 21;
    public static int FRM_TALON2 = 20;
    public static int FB_TALON_ROLL = 44;
    public static int FUD_TALON = 38;
    public static int BSL_TALON = 46;
    public static int BSR_TALON = 47;
    public static int CW_TALON = 30;
    public static int FB_TALON = 45;
    public static int LS_TALON = 61;
    public static int RS_TALON = 60;
    public static int UDSM_TALON = 62;
    public static int DW_TALON = 42;

    // LED stuff
    public static int LED_AMOUNT = 51;
    public static int PWM_LEDS = 9;
    public static int PWM_LEDS2 = 8;

    // Controller buttons
    public static int XBOX_CONTROLLER = 0;
    public static int SECONDARY_XBOX_CONTROLLER = 1;
    public static int DEATH_WHEEL_BUTTON = 1;
    public static int FEEDER_EMERGENCY_STOP = 2;
    public static int SHOOTER_BUTTON = 3;
    public static int FEEDER_BAR_BUTTON = 6;
    public static int FEEDER_QUEUE_BUTTON = 7;
    public static int COLOR_READ_BUTTON = 8;
    public static int FINESSE_BUTTON = 9;

    // DIO switches
    public static int CAMERA_SERVO = 0; // this one is actually a pwm
    public static int SHOOTER_ASSIST_SERVO = 1;
    public static int SHOOTY_SWITCH = 4;
    public static int FEEDER_FRONT_FIRST_SWITCH = 6;
    public static int FEEDER_FRONT_SECOND_SWITCH = 7;
    public static int FEEDER_BACK_FIRST_SWITCH = 8;
    public static int FEEDER_BACK_SECOND_SWITCH = 9;
    public static int WHEEL_OUT_SWITCH = 0;
    public static int WHEEL_OUT_SWITCH_TWO = 1;

    // Network Tables
    public static String ADVANCE_KEY = "shouldBallAdvance";
    public static String DW_COUNT_TABLE_KEY = "ballsDeathWheel";
    public static String ADD_TO_DW_COUNT = "iefhoa";
    public static String FEEDER_COUNT_TABLE_KEY = "ballsFeeder";
    public static String FEEDER_DEATHWHEEL_KEY = "aawfe";
    public static String GLOBAL_NETWORK_TABLE = "networkTable";
    public static String TOTAL_TABLE_KEY = "totalBalls";
    public static String IS_SHOOTER_SHOOTING_KEY = "shooterShooting";
    public static String IS_SHOOTER_STUCK_KEY = "shooterIsStuck";
    public static String REVERSE_ALL_KEY = "reverseEverything";

    // Controller Mapping
    public enum XBOX_BUTTON {
        XBOX_A_BUTTON(1), XBOX_B_BUTTON(2), XBOX_X_BUTTON(3), XBOX_Y_BUTTON(4), XBOX_LEFT_SHOLDER_BUTTON(5),
        XBOX_RIGHT_SHOLDER_BUTTON(6), XBOX_BACK_BUTTON(7), XBOX_START_BUTTON(8), XBOX_LEFT_INDEX_TRIGGER(9),
        XBOX_RIGHT_INDEX_TRIGGER(10);

        private final int value;

        XBOX_BUTTON(final int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    } 
    

    public enum XBOX_AXIS {
        XBOX_LEFT_STICK_X_AXIS(0), XBOX_LEFT_STICK_Y_AXIS(1), XBOX_LEFT_TRIGGER_AXIS(2), XBOX_RIGHT_TRIGGER_AXIS(3),
        XBOX_RIGHT_STICK_X_AXIS(4), XBOX_RIGHT_STICK_Y_AXIS(5);

        private final int value;

        XBOX_AXIS(final int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    public enum XBOX_POV {
        XBOX_DPAD_RIGHT_ANGLE(90), XBOX_DPAD_LEFT_ANGLE(270), XBOX_DPAD_UP_ANGLE(0), XBOX_DPAD_DOWN_ANGLE(180);

        private final int value;

        XBOX_POV(final int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }

    }

    // Game rules
    public static int MAX_NUM_BALLS_ALLOWED_HOLDING = 5;
}