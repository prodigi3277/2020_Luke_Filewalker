/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.*;

public class ReverseEverything extends CommandBase {
 private NetworkTableEntry shouldThingsBeReversing;
 private NetworkTableInstance tableInstance;
 private final Feeder m_feeder;
 private  final DeathWheel m_beyblade;
  /**
   * Creates a new ReverseEverything.
   */
  public ReverseEverything(Feeder feeder, DeathWheel beyblade) {
    m_feeder = feeder;
    m_beyblade = beyblade;
     addRequirements(m_beyblade, m_feeder); 


    tableInstance = NetworkTableInstance.getDefault();
    NetworkTable table = tableInstance.getTable(Constants.GLOBAL_NETWORK_TABLE);
   shouldThingsBeReversing = table.getEntry(Constants.REVERSE_ALL_KEY);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    shouldThingsBeReversing.setBoolean(true);
    m_feeder.reverseEverythingFeeder();
    m_beyblade.reverseEverythingDeathWheel();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    shouldThingsBeReversing.setBoolean(false);
    m_feeder.stopThingsReversingFeeder();
    m_beyblade.stopThingsReversingDeathWheel();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
