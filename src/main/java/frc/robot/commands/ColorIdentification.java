/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.MeSeeColorGood;

public class ColorIdentification extends CommandBase {
  private final MeSeeColorGood  m_colorSpin; 
  private boolean endCommand = false;
 
  /**
   * Creates a new ColorIdentification.
   */
  public ColorIdentification(MeSeeColorGood colorSpin) {
    m_colorSpin = colorSpin;


    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(colorSpin);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    endCommand = m_colorSpin.wheelSpinnyColor();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return endCommand;
  }
}
