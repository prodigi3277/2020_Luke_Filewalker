/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.TankDrive;

public class AutonCommand extends CommandBase {
  private final TankDrive m_tankDrive;
  private static double AUTON_TICKS = 2501;
  private double currentTicks = 0;
  private boolean quitAuton = false;
  private double startingTicks;
  /**
   * Creates a new autonCommand.
   */
  public AutonCommand(TankDrive tankDrive) {
    m_tankDrive = tankDrive;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(m_tankDrive);
  }


  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    startingTicks = m_tankDrive.autonPathDriveForward();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if(currentTicks > startingTicks +AUTON_TICKS ){
      quitAuton = true;
      m_tankDrive.autonPathStop();
    }
    
    if(!quitAuton)
    {
    currentTicks = m_tankDrive.autonPathDriveForward();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    m_tankDrive.autonPathStop();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return quitAuton;
  }
  
}
