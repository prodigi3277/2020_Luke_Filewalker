/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.TankDrive;
import frc.robot.subsystems.LightAmplificationbySimulatedEmissionofRadiation;

public class VroomVroom extends CommandBase {
  private final TankDrive m_tankDrive;
  private final LightAmplificationbySimulatedEmissionofRadiation m_lights;
  private DoubleSupplier m_leftSpeed;
  private DoubleSupplier m_rightSpeed;

  public VroomVroom(TankDrive tankDrive, LightAmplificationbySimulatedEmissionofRadiation lights,
      DoubleSupplier leftSpeed, DoubleSupplier rightSpeed) {
    m_tankDrive = tankDrive;
    m_lights = lights;
    m_leftSpeed = leftSpeed;
    m_rightSpeed = rightSpeed;

    addRequirements(m_tankDrive);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double rightSpeed = m_rightSpeed.getAsDouble();
    double leftSpeed = m_leftSpeed.getAsDouble();

    if (leftSpeed > 0) {
      m_lights.setRGBForward();
     // m_lights.setRGBForward2();
    } else if (leftSpeed < 0) {
      m_lights.setRGBReverse();
     // m_lights.setRGBReverse2();
    } else {
      m_lights.setTeamColor();
      //m_lights.setTeamColor2();
    }

    m_tankDrive.DriveWithController(leftSpeed, rightSpeed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() { 
    return false;
  }
}
