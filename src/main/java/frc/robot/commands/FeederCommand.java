/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Feeder;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

public class FeederCommand extends CommandBase {
  private Feeder m_feeder = new Feeder();
  private NetworkTableInstance tableInstance;
  private NetworkTableEntry ballsInFeederEntry;
  private NetworkTableEntry ballsInDeathWheelEntry;
  private NetworkTableEntry totalBallCountEntry;
  private double feederBallCount;
  private double deathWheelCount;
  private double totalBallCount;
 
  public FeederCommand(Feeder feeder) {
   m_feeder = feeder;

   tableInstance = NetworkTableInstance.getDefault();
   NetworkTable table = tableInstance.getTable(Constants.GLOBAL_NETWORK_TABLE);
   
   // Do this just to publish the entry asap
   ballsInFeederEntry = table.getEntry(Constants.FEEDER_COUNT_TABLE_KEY);
   ballsInDeathWheelEntry = table.getEntry(Constants.DW_COUNT_TABLE_KEY);
   totalBallCountEntry = table.getEntry(Constants.TOTAL_TABLE_KEY);

  addRequirements(m_feeder);
  }

  // Called when the command is  initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    updateTotalBallCount();
    m_feeder.feederBeltControl();
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }

  private void updateTotalBallCount()
  {
    deathWheelCount = ballsInDeathWheelEntry.getDouble(0);
    feederBallCount = ballsInFeederEntry.getDouble(0);
    totalBallCount = feederBallCount + deathWheelCount;
    totalBallCountEntry.setDouble(totalBallCount);
  }
}
